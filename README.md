# SpringBoot医院信息管理系统

#### 介绍
springboot+thymeleaf医院信息管理系统，无代码缺失。 该项目主要功能模块为1门诊模块，该模块主要功能有，门诊挂号，药品划价，项目划价，项目缴费等功能。2住院模块，该模块功能主要有，住院登记，药品划价，出院结算等功能。具体功能可以参考以下功能图

![图片21](https://gitee.com/call-me-orange-peel/orange-hospital-project/raw/master/asset/图片21.png)

![图片22](https://gitee.com/call-me-orange-peel/orange-hospital-project/raw/master/asset/图片22.png)



若对该项目感兴趣，希望各位大牛能够参与到项目的建设中，让项目更加完善，为新人提供一些有价值的学习项目。

（仅供参考，如有问题还请见谅！如果觉得不错的请帮忙推荐一下谢谢各位帅哥美女！）



#### 注意事项

sql脚本，还有登陆的账号密码都以放入到resource目录下的lib目录

<img src="https://gitee.com/call-me-orange-peel/orange-hospital-project/raw/master/asset/iShot_2024-03-06_19.13.00.png" alt="iShot_2024-03-06_19.13.00" style="zoom:50%;" />

#### 具体图
![图片12](https://gitee.com/call-me-orange-peel/orange-hospital-project/raw/master/asset/图片12.png)

![图片1](https://gitee.com/call-me-orange-peel/orange-hospital-project/raw/master/asset/图片1.png)

![图片2](https://gitee.com/call-me-orange-peel/orange-hospital-project/raw/master/asset/图片2.png)

![图片3](https://gitee.com/call-me-orange-peel/orange-hospital-project/raw/master/asset/图片3.png)

![图片4](https://gitee.com/call-me-orange-peel/orange-hospital-project/raw/master/asset/图片4.png)

![图片5](https://gitee.com/call-me-orange-peel/orange-hospital-project/raw/master/asset/图片5.png)

![图片6](https://gitee.com/call-me-orange-peel/orange-hospital-project/raw/master/asset/图片6.png)

![图片7](/Users/orange/Documents/z_临时/img/图片7.png)

![图片8](https://gitee.com/call-me-orange-peel/orange-hospital-project/raw/master/asset/图片8.png)

![图片9](https://gitee.com/call-me-orange-peel/orange-hospital-project/raw/master/asset/图片9.png)

![图片10](https://gitee.com/call-me-orange-peel/orange-hospital-project/raw/master/asset/图片10.png)

![图片11](https://gitee.com/call-me-orange-peel/orange-hospital-project/raw/master/asset/图片11.png)

![图片13](https://gitee.com/call-me-orange-peel/orange-hospital-project/raw/master/asset/图片13.png)

![图片14](https://gitee.com/call-me-orange-peel/orange-hospital-project/raw/master/asset/图片14.png)
